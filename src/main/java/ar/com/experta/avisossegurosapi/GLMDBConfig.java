package ar.com.experta.avisossegurosapi;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "glmEntityManagerFactory",
        transactionManagerRef = "glmTransactionManager",
        basePackages = { "ar.com.experta.avisossegurosapi.infraestructure.dbglm" }
)
public class GLMDBConfig {

    @Bean(name="glmDataSource")
    @Primary
    @ConfigurationProperties(prefix="spring.glmdatasource")
    public DataSource glmDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "glmEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean glmEntityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                              @Qualifier("glmDataSource") DataSource glmDataSource) {
        return builder
                .dataSource(glmDataSource)
                .packages("ar.com.experta.avisossegurosapi.infraestructure.dbglm")
                .build();
    }

    @Bean(name = "glmTransactionManager")
    public PlatformTransactionManager glmTransactionManager(
            @Qualifier("glmEntityManagerFactory") EntityManagerFactory glmEntityManagerFactory) {
        return new JpaTransactionManager(glmEntityManagerFactory);
    }
}