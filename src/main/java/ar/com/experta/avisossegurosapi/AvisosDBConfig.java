package ar.com.experta.avisossegurosapi;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "avisosEntityManagerFactory",
        transactionManagerRef = "avisosTransactionManager",
        basePackages = { "ar.com.experta.avisossegurosapi.infraestructure.dbavisos" }
)
public class AvisosDBConfig {

    @Bean(name="avisosDataSource")
    @ConfigurationProperties(prefix="spring.avisosdatasource")
    public DataSource avisosDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "avisosEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean avisosEntityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                                @Qualifier("avisosDataSource") DataSource avisosDataSource) {
        return builder
                .dataSource(avisosDataSource)
                .packages("ar.com.experta.avisossegurosapi.infraestructure.dbavisos")
                .build();
    }

    @Bean(name = "avisosTransactionManager")
    public PlatformTransactionManager avisosTransactionManager(
            @Qualifier("avisosEntityManagerFactory") EntityManagerFactory avisosEntityManagerFactory) {
        return new JpaTransactionManager(avisosEntityManagerFactory);
    }
}