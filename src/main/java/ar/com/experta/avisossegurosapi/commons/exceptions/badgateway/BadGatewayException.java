package ar.com.experta.avisossegurosapi.commons.exceptions.badgateway;

public class BadGatewayException extends RuntimeException {

    String detail;

    public BadGatewayException(String message, Exception originalException) {
        super(message);
        originalException.printStackTrace();
        this.detail = originalException.getLocalizedMessage();
    }

    public BadGatewayException(String message, String detail) {
        super(message);
        this.detail = detail;
    }

    public String getDetail() {
        return detail;
    }

}
