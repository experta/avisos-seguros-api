package ar.com.experta.avisossegurosapi.commons.exceptions;

public class EnvioEmailException extends Exception{
    public EnvioEmailException(String message) {
        super(message);
    }
}
