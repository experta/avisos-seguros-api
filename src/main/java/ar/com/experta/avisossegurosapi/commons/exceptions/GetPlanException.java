package ar.com.experta.avisossegurosapi.commons.exceptions;

public class GetPlanException extends Exception {

    public GetPlanException(String message) {
        super(message);
    }
}
