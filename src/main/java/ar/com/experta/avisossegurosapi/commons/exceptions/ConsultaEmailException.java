package ar.com.experta.avisossegurosapi.commons.exceptions;

public class ConsultaEmailException extends Exception{
    public ConsultaEmailException(String message) {
        super(message);
    }
}
