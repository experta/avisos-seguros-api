package ar.com.experta.avisossegurosapi.commons.exceptions;

public class URLSharingBadGatewayException extends Exception {
    public URLSharingBadGatewayException(String message) {
        super(message);
    }
}
