package ar.com.experta.avisossegurosapi.commons.exceptions.notfound;

public abstract class NotFoundException extends RuntimeException {

    protected NotFoundException(String message) {
        super(message);
    }

}
