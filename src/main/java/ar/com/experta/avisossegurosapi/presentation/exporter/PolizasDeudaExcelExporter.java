package ar.com.experta.avisossegurosapi.presentation.exporter;

import ar.com.experta.avisossegurosapi.core.model.PolizaConDeuda;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.modelmapper.internal.bytebuddy.implementation.bytecode.Throw;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PolizasDeudaExcelExporter {
    private XSSFWorkbook workbook;
    private XSSFSheet sheet;
    private List<PolizaConDeuda> polizaConDeudaList;

    public PolizasDeudaExcelExporter(List<PolizaConDeuda> polizaConDeudaList) {
        this.polizaConDeudaList = polizaConDeudaList;
        workbook = new XSSFWorkbook();
    }


    private void writeHeaderLine() {
        try {
            sheet = workbook.createSheet("Users");

            Row row = sheet.createRow(0);

            CellStyle style = workbook.createCellStyle();
            XSSFFont font = workbook.createFont();
            font.setBold(true);
            font.setFontHeight(16);
            style.setFont(font);

            createCell(row, 0, "Ramo", style);
            createCell(row, 1, "Numero de Póliza", style);
            createCell(row, 2, "Cuota", style);
            createCell(row, 3, "Cliente", style);
            createCell(row, 4, "Monto Adeudado", style);
            createCell(row, 5, "Fecha de Vencimiento", style);

        }catch(Exception e){
             new Exception(e.getMessage());
        }

    }

    private void createCell(Row row, int columnCount, Object value, CellStyle style) {

        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }else {
            cell.setCellValue((String) value);
        }
        cell.setCellStyle(style);

    }

    private void writeDataLines() {
        int rowCount = 1;

        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        CellStyle styleCurrency = workbook.createCellStyle();
        styleCurrency.setDataFormat((short)8);

        for (PolizaConDeuda polizaConDeuda : polizaConDeudaList) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;

            createCell(row, columnCount++, polizaConDeuda.getRamo().label(), style);
            createCell(row, columnCount++, polizaConDeuda.getNumero(), style);
            createCell(row, columnCount++, polizaConDeuda.getCuota().toString(), style);
            createCell(row, columnCount++, polizaConDeuda.getCliente().getNombre(), style);
            createCell(row, columnCount++, polizaConDeuda.getSaldo().toString(), style);
            createCell(row, columnCount++, polizaConDeuda.getFechaVencimiento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), style);

        }
    }

    public void export(HttpServletResponse response) throws IOException {
        writeHeaderLine();
        writeDataLines();

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
//        sheet.autoSizeColumn(0);
//        sheet.autoSizeColumn(1);
//        sheet.autoSizeColumn(2);
//        sheet.autoSizeColumn(3);
//        sheet.autoSizeColumn(4);
//        sheet.autoSizeColumn(5);
        workbook.close();

        outputStream.close();

    }
}