package ar.com.experta.avisossegurosapi.presentation;


import ar.com.experta.avisossegurosapi.core.AvisosService;
import ar.com.experta.avisossegurosapi.core.model.Aviso;
import ar.com.experta.avisossegurosapi.core.model.PolizaConDeuda;
import ar.com.experta.avisossegurosapi.presentation.exporter.PolizasDeudaExcelExporter;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Validated
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/avisos")
@Api(tags="Avisos de Experta Seguros", description="Endpoints para consultar estado de avisos de Experta Seguros")
public class AvisosController {

    @Autowired
    private AvisosService avisosService;

    @PostMapping(value = "/falta-pago-cuponera-clientes")
    @ApiOperation(value = "Enviar avisos de falta de pago de polizas con cuponera")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Proceso correcto"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity postFaltaPagoCuponeraClientes(){
        avisosService.postFaltaPagoCuponeraClientes();
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/falta-pago-cuponera-productores")
    @ApiOperation(value = "Enviar avisos de falta de pago de polizas con cuponera")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Proceso correcto"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity postFaltaPagoCuponeraProductores(){
        avisosService.postFaltaPagoCuponeraProductores();
        return ResponseEntity.noContent().build();
    }


    @GetMapping(value = "/reportefaltapagocuponera/{productor}.xlsx")
    @ApiOperation(value = "Obtener los avisos enviados")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Proceso correcto"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public void getPolizasConDeudaXLS(@ApiParam(name = "productor", value = "Id del vendedor del reporte que se desea obtener", required = true) @PathVariable String productor, HttpServletResponse response) throws IOException {

        response.setContentType("application/octet-stream");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=reporteDeudaCuponera.xlsx";
        response.setHeader(headerKey, headerValue);

        List<PolizaConDeuda> polizasConDeuda = avisosService.getPolizasConDeuda(productor);
        PolizasDeudaExcelExporter excelExporter = new PolizasDeudaExcelExporter(polizasConDeuda);
        excelExporter.export(response);
    }

    @GetMapping(value = "/reportefaltapagocuponera")
    @ApiOperation(value = "Obtener los avisos enviados")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Proceso correcto"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getPolizasConDeuda(@ApiParam(name = "productor", value = "Numero de productor", required = false) @RequestParam(required = false) String productor) {
        return ResponseEntity.ok(avisosService.getPolizasConDeuda(productor));
    }

    @GetMapping(value = "/")
    @ApiOperation(value = "Obtener los avisos enviados")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Proceso correcto"),
            @ApiResponse(code = 500, message = "Error interno"),
            @ApiResponse(code = 501, message = "No implementado"),
            @ApiResponse(code = 502, message = "Error de servicio relacionado")
    })
    public ResponseEntity getAvisos(){
        return ResponseEntity.ok(avisosService.getAvisos());
    }

}
