package ar.com.experta.avisossegurosapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvisosSegurosApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvisosSegurosApiApplication.class, args);
	}

}
