package ar.com.experta.avisossegurosapi.infraestructure.mensajeriaclient.model;

public class MensajeriaResponse {

    private String interaccion;
    private String estado;

    public MensajeriaResponse() {
    }

    public MensajeriaResponse(String interaccion, String estado) {
        this.interaccion = interaccion;
        this.estado = estado;
    }

    public String getInteraccion() {
        return interaccion;
    }

    public void setInteraccion(String interaccion) {
        this.interaccion = interaccion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
