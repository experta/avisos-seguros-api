package ar.com.experta.avisossegurosapi.infraestructure.dbavisos;

import ar.com.experta.avisossegurosapi.commons.exceptions.NotImplementedException;
import ar.com.experta.avisossegurosapi.core.model.*;
import ar.com.experta.avisossegurosapi.infraestructure.dbavisos.model.AvisoDb;
import ar.com.experta.avisossegurosapi.infraestructure.dbavisos.repository.AvisosRepository;
import ar.com.experta.avisossegurosapi.infraestructure.dbglm.model.RamoDb;
import ar.com.experta.avisossegurosapi.infraestructure.polizasclient.model.Poliza;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class AvisosDAO {

    @Autowired
    private AvisosRepository avisosRepository;

    public Boolean existeAvisoFaltaPagoCliente(PolizaConDeuda poliza){

        String codigo;

        switch (poliza.getFormaPago()){
            case CUPONERA:
                codigo = TipoAviso.DEUDA_POLIZAS_CUPONERA_CLIENTES.codigo();
                break;
            case OPERATORIA_BANCARIA:
                codigo = TipoAviso.DEUDA_POLIZAS_OPBANCARIA_CLIENTES.codigo();
                break;
            default:
                throw new NotImplementedException();
        }

        return avisosRepository.countAvisos(
                codigo,
                RamoDb.valueOf(poliza.getRamo().toString()).value(),
                Long.valueOf(poliza.getNumero()),
                poliza.getCuota()
        ).compareTo(0L) > 0;
    }

    public Boolean existeAvisoFaltaPagoProductor(Productor productor){
        return avisosRepository.countAvisos(
                TipoAviso.DEUDA_POLIZAS_PRODUCTOR.codigo(),
                Long.valueOf(productor.getId()),
                LocalDateTime.now().minusDays(TipoAviso.DEUDA_POLIZAS_PRODUCTOR.diasRepeticion())
        ).compareTo(0L) > 0;
    }

    public Aviso crearAvisoFaltaPagoCliente(PolizaConDeuda poliza){
        Aviso aviso = new AvisoFaltaPagoCliente(null, Estado.A_ENVIAR, null, LocalDateTime.now(), null, Integer.valueOf(1),poliza);
        AvisoDb avisoDb = new AvisoDb(aviso);
        avisosRepository.save(avisoDb);
        aviso.setId(avisoDb.getId().toString());
        return aviso;
    }

    public Aviso crearAvisoFaltaPagoProductor(Productor productor){
        Aviso aviso = new AvisoFaltaPagoProductor(null, Estado.A_ENVIAR, null, LocalDateTime.now(), null, Integer.valueOf(1), productor.getId());
        AvisoDb avisoDb = new AvisoDb(aviso);
        avisosRepository.save(avisoDb);
        aviso.setId(avisoDb.getId().toString());
        return aviso;
    }

    public void guardarAviso(Aviso aviso) {
        AvisoDb avisoDb = new AvisoDb(aviso);
        avisosRepository.save(avisoDb);
    }

    public Aviso getAvisoFaltaPagoCliente(PolizaConDeuda poliza) {

        String codigo;
        switch (poliza.getFormaPago()){
            case CUPONERA:
                codigo = TipoAviso.DEUDA_POLIZAS_CUPONERA_CLIENTES.codigo();
                break;
            case OPERATORIA_BANCARIA:
                codigo = TipoAviso.DEUDA_POLIZAS_OPBANCARIA_CLIENTES.codigo();
                break;
            default:
                throw new NotImplementedException();
        }
        List<AvisoDb> avisosDb = avisosRepository.getAvisos(codigo,
                RamoDb.valueOf(poliza.getRamo().toString()).value(),
                Long.valueOf(poliza.getNumero()),
                poliza.getCuota());

        return avisosDb != null && avisosDb.size() > 0 ? avisosDb.get(0).mapToAviso() : null;
    }

    public Aviso getAvisoFaltaPagoProductor(Productor productor) {
        List<AvisoDb> avisosDb = avisosRepository.getAvisos(TipoAviso.DEUDA_POLIZAS_PRODUCTOR.codigo(),
                Long.valueOf(productor.getId()),
                LocalDateTime.now().minusDays(TipoAviso.DEUDA_POLIZAS_PRODUCTOR.diasRepeticion()));

        return avisosDb != null && avisosDb.size() > 0 ? avisosDb.get(0).mapToAviso() : null;
    }

    public List<Aviso> findAvisos(){
        return avisosRepository.getAll().stream().map(a -> a.mapToAviso()).collect(Collectors.toList());
    }

    public List<Aviso> findAvisosPorEstado(Estado estado) {
        return avisosRepository.getAllByEstado(estado).stream().map(a -> a.mapToAviso()).collect(Collectors.toList());
    }

}
