package ar.com.experta.avisossegurosapi.infraestructure.dbglm.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PagoVencidoId implements Serializable {

    @Column(name = "codrama", length = 2)
    private Integer ramo;

    @Column(name = "poliza", length = 8)
    private Long numero;

    @Column(name = "nrocuota", length = 2)
    private Integer cuota;

    public PagoVencidoId() {
    }

    public Integer getRamo() {
        return ramo;
    }

    public void setRamo(Integer ramo) {
        this.ramo = ramo;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PagoVencidoId)) return false;
        PagoVencidoId that = (PagoVencidoId) o;
        return Objects.equals(getRamo(), that.getRamo()) && Objects.equals(getNumero(), that.getNumero()) && Objects.equals(getCuota(), that.getCuota());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRamo(), getNumero(), getCuota());
    }
}
