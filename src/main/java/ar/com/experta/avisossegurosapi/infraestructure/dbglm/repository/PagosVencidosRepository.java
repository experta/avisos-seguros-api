package ar.com.experta.avisossegurosapi.infraestructure.dbglm.repository;

import ar.com.experta.avisossegurosapi.core.model.Productor;
import ar.com.experta.avisossegurosapi.infraestructure.dbglm.model.PagoVencidoDB;
import ar.com.experta.avisossegurosapi.infraestructure.dbglm.model.PagoVencidoId;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface PagosVencidosRepository extends CrudRepository<PagoVencidoDB, PagoVencidoId> {

    @Query( " FROM PagoVencidoDB p WHERE (:idProductor is null OR p.idProductor = :idProductor) AND codrama = 1 " )
    List<PagoVencidoDB> findPolizasConDeuda (@Param("idProductor") Long idProductor);

    interface Productor{
        Long getId();
        String getNombre();
        String getEmail();
    }

    @Query( value = " SELECT DISTINCT codproductor id, nombreproductor nombre, mailproductor email" +
            "   FROM glm_experta.v_aviso_no_pago WHERE vtocuota > sysdate - :dias AND codrama = 1", nativeQuery = true)
    List<Productor> findProductoresConPolizasConDeuda(@Param("dias") Long dias);

}
