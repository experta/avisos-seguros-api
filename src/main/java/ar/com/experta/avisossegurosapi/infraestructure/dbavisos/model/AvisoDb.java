package ar.com.experta.avisossegurosapi.infraestructure.dbavisos.model;

import ar.com.experta.avisossegurosapi.core.model.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "AVISOS")
public class AvisoDb {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence")
//    @SequenceGenerator(name = "id_Sequence", sequenceName = "ID_SEQ")
    private Long id;

    @Column(name = "tipo", length = 4)
    private String tipo;

    @Column(name = "ramo", length = 2)
    private Integer ramo;

    @Column(name = "numero", length = 8)
    private Long numero;

    @Column(name = "cuota", length = 2)
    private Integer cuota;

    @Column(name = "productor", length = 7)
    private Long productor;

    @Column(name = "fecha")
    private LocalDateTime fecha;

    @Column(name = "estado", length = 10)
    @Enumerated(EnumType.STRING)
    private Estado estado;

    @Column(name = "id_mensajeria", length = 11)
    private Long idMensajeria;

    @Column(name = "error", length = 1000)
    private String error;

    @Column(name = "reintento", length = 2)
    private Integer reintento;

    public AvisoDb() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getRamo() {
        return ramo;
    }

    public void setRamo(Integer ramo) {
        this.ramo = ramo;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    public Long getProductor() {
        return productor;
    }

    public void setProductor(Long productor) {
        this.productor = productor;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public Long getIdMensajeria() {
        return idMensajeria;
    }

    public void setIdMensajeria(Long idMensajeria) {
        this.idMensajeria = idMensajeria;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Integer getReintento() {
        return reintento;
    }

    public void setReintento(Integer reintento) {
        this.reintento = reintento;
    }

    public AvisoDb(Aviso aviso){
        this.id = aviso.getId() != null ? Long.valueOf(aviso.getId()) : null;
        this.tipo = aviso.getTipo().codigo();
        this.estado = aviso.getEstado();
        this.fecha = aviso.getFecha() != null ? aviso.getFecha() : LocalDateTime.now();
        this.idMensajeria = aviso.getIdMensajeria() != null ? Long.valueOf(aviso.getIdMensajeria()) : null;
        this.error = aviso.getError();
        this.reintento = aviso.getReintento();
        switch (aviso.getTipo()){
            case DEUDA_POLIZAS_OPBANCARIA_CLIENTES:
            case DEUDA_POLIZAS_CUPONERA_CLIENTES:
                this.ramo = RamoDb.valueOf(((AvisoFaltaPagoCliente) aviso).getPolizaConDeuda().getRamo().toString()).value();
                this.numero = Long.valueOf(((AvisoFaltaPagoCliente) aviso).getPolizaConDeuda().getNumero());
                this.cuota = ((AvisoFaltaPagoCliente) aviso).getPolizaConDeuda().getCuota();
                break;
            case DEUDA_POLIZAS_PRODUCTOR:
                this.productor = Long.valueOf(((AvisoFaltaPagoProductor) aviso).getIdProductor());
                break;
        }
    }

    public Aviso mapToAviso(){
        TipoAviso tipoAviso = TipoAviso.fromValue(tipo);
        switch (tipoAviso){
            case DEUDA_POLIZAS_CUPONERA_CLIENTES:
                return new AvisoFaltaPagoCliente( this.id != null ? this.id.toString() : null,
                        this.estado,
                        this.idMensajeria != null ? this.idMensajeria.toString() : null,
                        this.fecha,
                        this.error,
                        this.reintento,
                        new PolizaConDeuda(Ramo.valueOf(RamoDb.fromValue(this.ramo).toString()) ,
                                this.numero.toString(),
                                this.cuota,null,null,null,null,FormaPago.CUPONERA));
            case DEUDA_POLIZAS_OPBANCARIA_CLIENTES:
                return new AvisoFaltaPagoCliente( this.id != null ? this.id.toString() : null,
                        this.estado,
                        this.idMensajeria != null ? this.idMensajeria.toString() : null,
                        this.fecha,
                        this.error,
                        this.reintento,
                        new PolizaConDeuda(Ramo.valueOf(RamoDb.fromValue(this.ramo).toString()) ,
                                this.numero.toString(),
                                this.cuota,null,null,null,null,FormaPago.OPERATORIA_BANCARIA));
            case DEUDA_POLIZAS_PRODUCTOR:
                return new AvisoFaltaPagoProductor(this.id != null ? this.id.toString() : null,
                        this.estado,
                        this.idMensajeria != null ? this.idMensajeria.toString() : null,
                        this.fecha,
                        this.error,
                        this.reintento,
                        this.productor.toString());
        }
        return null;
    }


}
