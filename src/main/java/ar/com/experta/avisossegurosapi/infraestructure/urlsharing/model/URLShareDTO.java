package ar.com.experta.avisossegurosapi.infraestructure.urlsharing.model;

public class URLShareDTO {

    private String url;

    public URLShareDTO() {
    }

    public URLShareDTO(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
