package ar.com.experta.avisossegurosapi.infraestructure.polizasclient;

import ar.com.experta.avisossegurosapi.core.model.Ramo;
import ar.com.experta.avisossegurosapi.infraestructure.polizasclient.model.Poliza;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class PolizasClient {

    @Value("${url_polizas}")
    private String HOST_POLIZAS_API;

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    public String getPlan(Ramo ramo, String numero) {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(HOST_POLIZAS_API + "/polizas/" + ramo.toString() + "/" + numero);
            return restTemplate.getForObject(builder.toUriString(), Poliza.class).getPlan();
        }catch (Exception e){
            return ramo.label();
        }
    }

}
