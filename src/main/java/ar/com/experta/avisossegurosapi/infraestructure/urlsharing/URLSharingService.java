package ar.com.experta.avisossegurosapi.infraestructure.urlsharing;

import ar.com.experta.avisossegurosapi.commons.exceptions.URLSharingBadGatewayException;
import ar.com.experta.avisossegurosapi.infraestructure.urlsharing.model.URLShareDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;

@Component
public class URLSharingService {

    @Value("${url_urlsharing}")
    private String HOST_URLSHARING;

    private static final String POST_URLSHARING = "/urls";

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    public URI crearURLSharing(String url) throws URLSharingBadGatewayException {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(HOST_URLSHARING + POST_URLSHARING);

            URLShareDTO requestEcommerce = new URLShareDTO(url);

            ResponseEntity<byte[]> response = restTemplate.exchange(builder.toUriString(), HttpMethod.POST, new HttpEntity<>(requestEcommerce), byte[].class, "1");

            if (response.getStatusCode().equals(HttpStatus.CREATED))
                return response.getHeaders().getLocation();

            throw new URLSharingBadGatewayException("Response: " + response.getStatusCode());

        } catch (Exception e) {
            throw new URLSharingBadGatewayException(e.getMessage());
        }
    }
}
