package ar.com.experta.avisossegurosapi.infraestructure.dbavisos.model;

import java.util.HashMap;
import java.util.Map;

public enum RamoDb {

    VIDA (1),
    SCVO (23),
    SEPELIO (2),
    SALUD (8),
    SALDO_DEUDOR (14),
    AGRO (7),
    AP (3),
    ROBO (6),
    HOGAR (15),
    AUTOS (4),
    VARIOS (16),
    COMERCIO (22),
    CAUCION (10),
    MOTOS (25)
    ;

    private static Map<Integer, RamoDb> mapRamoDb = new HashMap<>();

    static {
        for (RamoDb ramoDb : RamoDb.values()) {
            mapRamoDb.put(ramoDb.value(), ramoDb);
        }
    }
    private int value;

    RamoDb(final Integer value) {
        this.value = value;
    }

    public Integer value () {
        return Integer.valueOf(value);
    }

    public static RamoDb fromValue(final Integer value) {
        return mapRamoDb.get(value);
    }



}
