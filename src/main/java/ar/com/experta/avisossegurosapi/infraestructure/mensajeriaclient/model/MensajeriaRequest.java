package ar.com.experta.avisossegurosapi.infraestructure.mensajeriaclient.model;

public class MensajeriaRequest {
    private String destinatario;
    private String tipoEmail;
    private ParametrosMensajeria parametros;

    public MensajeriaRequest() {
    }

    public MensajeriaRequest(String destinatario, String tipoEmail, ParametrosMensajeria parametros) {
        this.destinatario = destinatario;
        this.tipoEmail = tipoEmail;
        this.parametros = parametros;
    }

    public String getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(String destinatario) {
        this.destinatario = destinatario;
    }

    public String getTipoEmail() {
        return tipoEmail;
    }

    public void setTipoEmail(String tipoEmail) {
        this.tipoEmail = tipoEmail;
    }

    public ParametrosMensajeria getParametros() {
        return parametros;
    }

    public void setParametros(ParametrosMensajeria parametros) {
        this.parametros = parametros;
    }
}
