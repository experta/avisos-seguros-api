package ar.com.experta.avisossegurosapi.infraestructure.dbglm.model;

import ar.com.experta.avisossegurosapi.core.model.*;
import ar.com.experta.avisossegurosapi.infraestructure.dbavisos.model.RamoDb;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(schema = "glm_experta", name = "v_aviso_no_pago")
@Immutable
public class PagoVencidoDB {

    @EmbeddedId
    private PagoVencidoId pagoVencidoId;

    @Column(name = "fechaenvio")
    private LocalDate fechaEnvio;

    @Column(name = "vtocuota")
    private LocalDate fechaVencimiento;

    @Column(name = "saldovencido")
    private BigDecimal saldo;

    @Column(name = "tomador", length = 60)
    private String nombreCliente;

    @Column(name = "mailtomador", length = 70)
    private String mailCliente;

    @Column(name = "codproductor", length = 7)
    private Long idProductor;

    @Column(name = "nombreproductor", length = 60)
    private String nombreProductor;

    @Column(name = "mailproductor", length = 70)
    private String mailProductor;

    @Column(name = "formapago", length = 2)
    private Integer formaPago;


    public PagoVencidoDB() {
    }

    public PagoVencidoId getPagoVencidoId() {
        return pagoVencidoId;
    }

    public void setPagoVencidoId(PagoVencidoId pagoVencidoId) {
        this.pagoVencidoId = pagoVencidoId;
    }

    public LocalDate getFechaEnvio() {
        return fechaEnvio;
    }

    public void setFechaEnvio(LocalDate fechaEnvio) {
        this.fechaEnvio = fechaEnvio;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getMailCliente() {
        return mailCliente;
    }

    public void setMailCliente(String mailCliente) {
        this.mailCliente = mailCliente;
    }

    public Long getIdProductor() {
        return idProductor;
    }

    public void setIdProductor(Long idProductor) {
        this.idProductor = idProductor;
    }

    public String getNombreProductor() {
        return nombreProductor;
    }

    public void setNombreProductor(String nombreProductor) {
        this.nombreProductor = nombreProductor;
    }

    public String getMailProductor() {
        return mailProductor;
    }

    public void setMailProductor(String mailProductor) {
        this.mailProductor = mailProductor;
    }

    public Integer getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(Integer formaPago) {
        this.formaPago = formaPago;
    }

    public PolizaConDeuda mapToPolizaConDeuda() {
        return new PolizaConDeuda(
                Ramo.valueOf(RamoDb.fromValue(this.pagoVencidoId.getRamo()).toString()),
                this.pagoVencidoId.getNumero().toString(),
                this.pagoVencidoId.getCuota(),
                this.fechaVencimiento,
                this.saldo,
                new Cliente(this.nombreCliente,
                        this.mailCliente),
                new Productor(this.idProductor != null ? this.idProductor.toString() : null,
                        this.nombreProductor,
                        this.mailProductor),
                FormaPago.fromCodigo(this.formaPago));
    }

}
