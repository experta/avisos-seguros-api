package ar.com.experta.avisossegurosapi.infraestructure.dbavisos.repository;

import ar.com.experta.avisossegurosapi.core.model.Aviso;
import ar.com.experta.avisossegurosapi.core.model.Estado;
import ar.com.experta.avisossegurosapi.infraestructure.dbavisos.model.AvisoDb;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface AvisosRepository extends CrudRepository<AvisoDb, String> {

    @Query( " SELECT COUNT(a) FROM AvisoDb a " +
            " WHERE (:tipo is null OR a.tipo = :tipo) " +
            " AND (:ramo is null OR a.ramo = :ramo) " +
            " AND (:numero is null OR a.numero = :numero) " +
            " AND (:cuota is null OR a.cuota = :cuota) ")
    Long countAvisos (@Param("tipo") String tipo,
                        @Param("ramo") Integer ramo,
                        @Param("numero") Long numero,
                        @Param("cuota") Integer cuota);

    @Query( " FROM AvisoDb a " +
            " WHERE (:tipo is null OR a.tipo = :tipo) " +
            " AND (:ramo is null OR a.ramo = :ramo) " +
            " AND (:numero is null OR a.numero = :numero) " +
            " AND (:cuota is null OR a.cuota = :cuota) ")
    List<AvisoDb> getAvisos (@Param("tipo") String tipo,
                      @Param("ramo") Integer ramo,
                      @Param("numero") Long numero,
                      @Param("cuota") Integer cuota);

    @Query( " SELECT COUNT(a) FROM AvisoDb a " +
            " WHERE (:tipo is null OR a.tipo = :tipo) " +
            " AND (:productor is null OR a.productor = :productor) " +
            " AND (:fechaDesde is null OR a.fecha > :fechaDesde) ")
    Long countAvisos (@Param("tipo") String tipo,
                      @Param("productor") Long productor,
                      @Param("fechaDesde") LocalDateTime fechaDesde);

    @Query( " FROM AvisoDb a " +
            " WHERE (:tipo is null OR a.tipo = :tipo) " +
            " AND (:productor is null OR a.productor = :productor) " +
            " AND (:fechaDesde is null OR a.fecha > :fechaDesde) ")
    List<AvisoDb> getAvisos (@Param("tipo") String tipo,
                      @Param("productor") Long productor,
                      @Param("fechaDesde") LocalDateTime fechaDesde);

    @Query( " FROM AvisoDb a " )
    List<AvisoDb> getAll();

    @Query( " FROM AvisoDb a WHERE (:estado is null OR a.estado = :estado)" )
    List<AvisoDb> getAllByEstado(@Param("estado") Estado estado);


}
