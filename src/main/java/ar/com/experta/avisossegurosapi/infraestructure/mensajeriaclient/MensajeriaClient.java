package ar.com.experta.avisossegurosapi.infraestructure.mensajeriaclient;

import ar.com.experta.avisossegurosapi.commons.exceptions.ConsultaEmailException;
import ar.com.experta.avisossegurosapi.commons.exceptions.EnvioEmailException;
import ar.com.experta.avisossegurosapi.core.model.Estado;
import ar.com.experta.avisossegurosapi.infraestructure.mensajeriaclient.model.MensajeriaRequest;
import ar.com.experta.avisossegurosapi.infraestructure.mensajeriaclient.model.MensajeriaResponse;
import ar.com.experta.avisossegurosapi.infraestructure.mensajeriaclient.model.ParametrosMensajeria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class MensajeriaClient {

    public static final String TIPO_MAIL_FALTA_PAGO_CUPONERA_CLIENTE = "AVISO_DE_NO_PAGO_CUPONERA_SEGUROS";
    public static final String TIPO_MAIL_FALTA_PAGO_OPBANCARIA_CLIENTE = "AVISO_DE_NO_PAGO_OPBANC_SEGUROS";
    public static final String TIPO_MAIL_FALTA_PAGO_PRODUCTORES = "REPORTE_NO_PAGO_PAS_SEGUROS";

    @Value("${url_mensajeria}")
    private String HOST_MENSAJERIA_API;

    private static final String POST_MAIL = "/emails";
    private static final String GET_MAIL = "/emails/{id}";

    @Autowired
    @Qualifier("restTemplate")
    private RestTemplate restTemplate;

    public String enviarMailFaltaPagoClientes(String tipo, String mail, String numero, String plan, String nombre, LocalDate fechaVencimiento, BigDecimal saldo) throws EnvioEmailException {
        MensajeriaRequest request = new MensajeriaRequest(
                mail,
                tipo,
                new ParametrosMensajeria(
                        numero,
                        nombre,
                        plan,
                        fechaVencimiento.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                        "$ " + saldo.toString()));
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(HOST_MENSAJERIA_API + POST_MAIL);
        try {
            URI uri =  restTemplate.postForLocation(builder.toUriString(), request);
            String path = uri.getPath();
            return path.substring(path.lastIndexOf('/') + 1);
        } catch (Exception e) {
            throw new EnvioEmailException(e.getMessage());
        }
    }

    public String enviarMailFaltaPagoCuponeraProductores(String mail, String nombre, String link) throws EnvioEmailException {
        MensajeriaRequest request = new MensajeriaRequest(
                mail,
                TIPO_MAIL_FALTA_PAGO_PRODUCTORES,
                new ParametrosMensajeria(
                        nombre,
                        LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                        LocalDate.now().plusDays(7).format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
                        link));
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(HOST_MENSAJERIA_API + POST_MAIL);
        try {
            URI uri =  restTemplate.postForLocation(builder.toUriString(), request);
            String path = uri.getPath();
            return path.substring(path.lastIndexOf('/') + 1);
        } catch (Exception e) {
            throw new EnvioEmailException(e.getMessage());
        }
    }


    public Estado getEstado(String idMensajeria) throws ConsultaEmailException {
        try {
            UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl((HOST_MENSAJERIA_API + GET_MAIL).replace("{id}", idMensajeria));
            MensajeriaResponse mensaje = restTemplate.getForObject(builder.toUriString(), MensajeriaResponse.class);

            if (mensaje.getEstado().equals("A_ENVIAR") || mensaje.getEstado().equals("REENVIO"))
                return Estado.A_ENVIAR;

            if (mensaje.getEstado().equals("ERROR_ENVIO") || mensaje.getEstado().equals("CANCELADO"))
                return Estado.FALLIDO;

            if (mensaje.getEstado().equals("ENVIADO"))
                return Estado.ENVIADO;

        }catch (Exception e){
            throw new ConsultaEmailException(e.getMessage());
        }
        throw new ConsultaEmailException("");
    }
}
