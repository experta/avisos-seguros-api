package ar.com.experta.avisossegurosapi.infraestructure.dbglm;

import ar.com.experta.avisossegurosapi.core.model.*;
import ar.com.experta.avisossegurosapi.infraestructure.dbglm.repository.PagosVencidosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class GlmDAO {

    @Autowired
    private PagosVencidosRepository pagosVencidosRepository;

    public List<PolizaConDeuda> obtenerPolizasConDeuda(String idProductor){
        return pagosVencidosRepository.findPolizasConDeuda(idProductor != null ? Long.valueOf(idProductor) : null)
                .stream()
                .map(pagoVencidoDB -> pagoVencidoDB.mapToPolizaConDeuda())
                .collect(Collectors.toList());
    }

    public List<Productor> obtenerProductoresConPolizasConDeuda() {
        return pagosVencidosRepository.findProductoresConPolizasConDeuda(TipoAviso.DEUDA_POLIZAS_PRODUCTOR.diasRepeticion())
                .stream()
                .map(productorDb -> new Productor(productorDb.getId().toString(), productorDb.getNombre(), productorDb.getEmail()))
                .collect(Collectors.toList());
    }
}
