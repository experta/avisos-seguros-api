package ar.com.experta.avisossegurosapi.infraestructure.mensajeriaclient.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ParametrosMensajeria {

    @JsonProperty("NRO_POLIZA")
    private String nroPoliza;
    @JsonProperty("NOMBRE")
    private String nombre;
    @JsonProperty("PLAN")
    private String plan;
    @JsonProperty("FECHA_VTO")
    private String fechaVencimiento;
    @JsonProperty("IMPORTE")
    private String importe;
    @JsonProperty("FECHA_VTO_DESDE")
    private String fechaVtoDesde;
    @JsonProperty("FECHA_VTO_HASTA")
    private String fechaVtoHasta;
    @JsonProperty("LINK_ADJUNTO")
    private String linkAdjunto;


    public ParametrosMensajeria() {
    }

    public ParametrosMensajeria(String nroPoliza, String nombre, String plan, String fechaVencimiento, String importe) {
        this.nroPoliza = nroPoliza;
        this.nombre = nombre;
        this.plan = plan;
        this.fechaVencimiento = fechaVencimiento;
        this.importe = importe;
    }

    public ParametrosMensajeria(String nombre, String fechaVtoDesde, String fechaVtoHasta, String linkAdjunto) {
        this.nombre = nombre;
        this.fechaVtoDesde = fechaVtoDesde;
        this.fechaVtoHasta = fechaVtoHasta;
        this.linkAdjunto = linkAdjunto;
    }

    public String getNroPoliza() {
        return nroPoliza;
    }

    public void setNroPoliza(String nroPoliza) {
        this.nroPoliza = nroPoliza;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getImporte() {
        return importe;
    }

    public void setImporte(String importe) {
        this.importe = importe;
    }

    public String getFechaVtoDesde() {
        return fechaVtoDesde;
    }

    public void setFechaVtoDesde(String fechaVtoDesde) {
        this.fechaVtoDesde = fechaVtoDesde;
    }

    public String getFechaVtoHasta() {
        return fechaVtoHasta;
    }

    public void setFechaVtoHasta(String fechaVtoHasta) {
        this.fechaVtoHasta = fechaVtoHasta;
    }

    public String getLinkAdjunto() {
        return linkAdjunto;
    }

    public void setLinkAdjunto(String linkAdjunto) {
        this.linkAdjunto = linkAdjunto;
    }
}
