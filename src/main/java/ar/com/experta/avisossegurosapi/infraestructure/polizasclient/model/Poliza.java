package ar.com.experta.avisossegurosapi.infraestructure.polizasclient.model;

public class Poliza {
    private String plan;

    public Poliza() {
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }
}
