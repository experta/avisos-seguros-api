package ar.com.experta.avisossegurosapi.infraestructure.filter;

import ar.com.experta.avisossegurosapi.infraestructure.filter.httpclient.RestTemplateCorrelationInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class RestClientCertConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestClientCertConfiguration.class);
    public static final int CONNECT_TIMEOUT = 10000;
    public static final int TIMEOUT = 20000;

    //   @Value("${trust.store.password}")
    //  private String trustStorePassword;

    @Autowired
    private RestTemplateCorrelationInterceptor correlationInterceptor;

    @Autowired
    private RestTemplateLoggerInterceptor restTemplateLoggerInterceptor;

    @Bean(name = "restTemplate")
    public RestTemplate regularRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();

        List<ClientHttpRequestInterceptor> interceptors
                = restTemplate.getInterceptors();

        if (CollectionUtils.isEmpty(interceptors)) {
            interceptors = new ArrayList<>();
        }

        interceptors.add(correlationInterceptor);
        interceptors.add(restTemplateLoggerInterceptor);
        restTemplate.setInterceptors(interceptors);

        return restTemplate;

    }

}