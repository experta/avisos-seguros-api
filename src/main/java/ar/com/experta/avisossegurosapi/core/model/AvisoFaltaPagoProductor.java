package ar.com.experta.avisossegurosapi.core.model;

import java.time.LocalDateTime;

public class AvisoFaltaPagoProductor extends Aviso{

    private String idProductor;

    public AvisoFaltaPagoProductor() {
        super(TipoAviso.DEUDA_POLIZAS_PRODUCTOR);
    }

    public AvisoFaltaPagoProductor(String id, Estado estado, String idMensajeria, LocalDateTime fecha, String error, Integer reintento, String idProductor) {
        super(id, TipoAviso.DEUDA_POLIZAS_PRODUCTOR, estado, idMensajeria, fecha, error, reintento);
        this.idProductor = idProductor;
    }

    public String getIdProductor() {
        return idProductor;
    }

    public void setIdProductor(String idProductor) {
        this.idProductor = idProductor;
    }
}
