package ar.com.experta.avisossegurosapi.core.model;

public enum Ramo {

    VIDA ("Vida"),
    SCVO ("SCVO"),
    SEPELIO ("Sepelio"),
    SALUD ("Salud"),
    SALDO_DEUDOR ("Saldo Deudor"),
    AGRO ("Agro"),
    AP ("Accidentes Personales"),
    ROBO ("Robo"),
    HOGAR ("Hogar"),
    AUTOS ("Autos"),
    VARIOS ("Riesgos Varios"),
    COMERCIO ("Integral de Comercio"),
    CAUCION ("Caución"),
    MOTOS ("Motos");

    private String label;

    Ramo(final String label) {
        this.label = label;
    }

    public String label () {
        return label;
    }

}
