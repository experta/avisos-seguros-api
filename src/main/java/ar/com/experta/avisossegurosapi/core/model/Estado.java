package ar.com.experta.avisossegurosapi.core.model;

public enum Estado {
    A_ENVIAR("Procesando"),
    FALLIDO ("Fallido"),
    ENVIADO ("Enviado");

    private String label;

    Estado(final String label) {
        this.label = label;
    }

    public String label () {
        return label;
    }

}
