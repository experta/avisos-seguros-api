package ar.com.experta.avisossegurosapi.core;

import ar.com.experta.avisossegurosapi.commons.exceptions.ConsultaEmailException;
import ar.com.experta.avisossegurosapi.commons.exceptions.EnvioEmailException;
import ar.com.experta.avisossegurosapi.commons.exceptions.URLSharingBadGatewayException;
import ar.com.experta.avisossegurosapi.core.model.*;
import ar.com.experta.avisossegurosapi.infraestructure.dbavisos.AvisosDAO;
import ar.com.experta.avisossegurosapi.infraestructure.dbglm.GlmDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AvisosService {

    private static final Logger logger = LoggerFactory.getLogger(AvisosService.class);
    private static final int MAX_REINTENTOS = 5;

    @Autowired
    private AvisosDAO avisosDAO;
    @Autowired
    private GlmDAO glmDAO;
    @Autowired
    private MensajeriaService mensajeriaService;

    private void sleepRandomly() {
        try {
            Thread.sleep((long) (Math.random() * 20_000));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException(e);
        }
    }

    private void actualizarEstadosAEnviar(){
        List<Aviso> avisosAEnviar = this.avisosDAO.findAvisosPorEstado(Estado.A_ENVIAR);

        if (avisosAEnviar != null && !avisosAEnviar.isEmpty()){
            for (Aviso aviso : avisosAEnviar){
                try {
                    Estado estado = mensajeriaService.getEstado(aviso.getIdMensajeria());
                    aviso.setEstado(estado);
                    this.avisosDAO.guardarAviso(aviso);
                }catch (ConsultaEmailException e){
                    logger.error("No se pudo actualizar el estado del mail: " + aviso.getIdMensajeria());
                }
            }
        }
    }

    public void postFaltaPagoCuponeraClientes() {
        logger.info("Proceso Falta de Pago de polizas con cuponera para Clientes");
        //Tiempo random para que cuando hay 2 instancias una comience antes q la otra. No me juzguen se hizo a las apuradas
        sleepRandomly();

        List<PolizaConDeuda> polizaConDeuda = glmDAO.obtenerPolizasConDeuda(null);

        for (PolizaConDeuda poliza : polizaConDeuda){
            if (!avisosDAO.existeAvisoFaltaPagoCliente(poliza)) {
                Aviso aviso = this.avisosDAO.crearAvisoFaltaPagoCliente(poliza);
                try {
                    String idMensajeria = mensajeriaService.enviarMailAvisoFaltaPagoCliente(poliza, aviso);
                    aviso.setIdMensajeria(idMensajeria);
                    aviso.setEstado(Estado.A_ENVIAR);
                    this.avisosDAO.guardarAviso(aviso);
                } catch (EnvioEmailException e) {
                    logger.error("No se pudo enviar el mail de falta de pago para la poliza " + poliza.getNumeroCompleto());
                    aviso.setEstado(Estado.FALLIDO);
                    aviso.setError(e.getMessage());
                    aviso.setReintento(1);
                    this.avisosDAO.guardarAviso(aviso);
                }
            }else{
                Aviso aviso = this.avisosDAO.getAvisoFaltaPagoCliente(poliza);
                if (aviso.getEstado().equals(Estado.A_ENVIAR)) {
                    try {
                        Estado estado = mensajeriaService.getEstado(aviso.getIdMensajeria());
                        if (estado.equals(Estado.ENVIADO)) {
                            aviso.setEstado(estado);
                            this.avisosDAO.guardarAviso(aviso);
                        } else if (estado.equals(Estado.FALLIDO)) {
                            aviso.incrementarReintento();
                            if (aviso.getReintento().intValue() < MAX_REINTENTOS) {
                                try {
                                    String idMensajeria = mensajeriaService.enviarMailAvisoFaltaPagoCliente(poliza, aviso);
                                    aviso.setIdMensajeria(idMensajeria);
                                    aviso.setEstado(Estado.A_ENVIAR);
                                } catch (EnvioEmailException e) {
                                    logger.error("No se pudo enviar el mail de falta de pago para la poliza " + poliza.getNumeroCompleto());
                                    aviso.setEstado(Estado.FALLIDO);
                                    aviso.setError(e.getMessage());
                                }
                            }
                            this.avisosDAO.guardarAviso(aviso);
                        }
                    } catch (ConsultaEmailException e) {
                        logger.error("No se pudo consultar el mail de falta de pago de la poliza " + poliza.getNumeroCompleto());
                    }
                } else if (aviso.getEstado().equals(Estado.FALLIDO) && aviso.getReintento().intValue() < MAX_REINTENTOS) {
                    aviso.incrementarReintento();
                    try {
                        String idMensajeria = mensajeriaService.enviarMailAvisoFaltaPagoCliente(poliza, aviso);
                        aviso.setIdMensajeria(idMensajeria);
                        aviso.setEstado(Estado.A_ENVIAR);
                    } catch (EnvioEmailException e) {
                        logger.error("No se pudo enviar el mail de falta de pago para la poliza " + poliza.getNumeroCompleto());
                        aviso.setEstado(Estado.FALLIDO);
                        aviso.setError(e.getMessage());
                    }
                    this.avisosDAO.guardarAviso(aviso);
                }
            }
        }
        actualizarEstadosAEnviar();
    }

    public void postFaltaPagoCuponeraProductores() {
        logger.info("Proceso Falta de Pago de polizas con cuponera para Productores");
        sleepRandomly();

        List<Productor> productoresConPolizasConDeuda = glmDAO.obtenerProductoresConPolizasConDeuda();

        //        for (Productor productor : productoresConPolizasConDeuda){
        Productor productor = productoresConPolizasConDeuda.get(0);
        productor.setEmail("mauroeramos@gmail.com");

            if (!avisosDAO.existeAvisoFaltaPagoProductor(productor)) {
                Aviso aviso = this.avisosDAO.crearAvisoFaltaPagoProductor(productor);
                try {
                    String idMensajeria = mensajeriaService.enviarMailAvisoFaltasPagoProductor(productor, aviso);
                    aviso.setIdMensajeria(idMensajeria);
                    aviso.setEstado(Estado.A_ENVIAR);
                    this.avisosDAO.guardarAviso(aviso);
                } catch (EnvioEmailException e) {
                    logger.error("No se pudo enviar el mail de avisos de falta de pago para el productor " + productor.getNombre());
                    aviso.setEstado(Estado.FALLIDO);
                    aviso.setError(e.getMessage());
                    aviso.setReintento(1);
                    this.avisosDAO.guardarAviso(aviso);
                } catch (URLSharingBadGatewayException e) {
                    logger.error("No se pudo crear el mail de avisos de falta de pago para el productor " + productor.getNombre());
                    aviso.setEstado(Estado.FALLIDO);
                    aviso.setError(e.getMessage());
                    aviso.setReintento(1);
                    this.avisosDAO.guardarAviso(aviso);
                }
            }else{
                Aviso aviso = this.avisosDAO.getAvisoFaltaPagoProductor(productor);
                if (aviso.getEstado().equals(Estado.A_ENVIAR)) {
                    try {
                        Estado estado = mensajeriaService.getEstado(aviso.getIdMensajeria());
                        if (estado.equals(Estado.ENVIADO)) {
                            aviso.setEstado(estado);
                            this.avisosDAO.guardarAviso(aviso);
                        } else if (estado.equals(Estado.FALLIDO)) {
                            aviso.incrementarReintento();
                            if (aviso.getReintento().intValue() < MAX_REINTENTOS) {
                                try {
                                    String idMensajeria = mensajeriaService.enviarMailAvisoFaltasPagoProductor(productor, aviso);
                                    aviso.setIdMensajeria(idMensajeria);
                                    aviso.setEstado(Estado.A_ENVIAR);
                                } catch (EnvioEmailException e) {
                                    logger.error("No se pudo enviar el mail de falta de pago para el productor " + productor.getNombre());
                                    aviso.setEstado(Estado.FALLIDO);
                                    aviso.setError(e.getMessage());
                                } catch (URLSharingBadGatewayException e) {
                                    logger.error("No se pudo crear el mail de avisos de falta de pago para el productor " + productor.getNombre());
                                    aviso.setEstado(Estado.FALLIDO);
                                    aviso.setError(e.getMessage());
                                    this.avisosDAO.guardarAviso(aviso);
                                }
                            }
                            this.avisosDAO.guardarAviso(aviso);
                        }
                    } catch (ConsultaEmailException e) {
                        logger.error("No se pudo consultar el mail para el productor " + productor.getNombre());
                    }
                } else if (aviso.getEstado().equals(Estado.FALLIDO) && aviso.getReintento().intValue() < MAX_REINTENTOS) {
                    aviso.incrementarReintento();
                    try {
                        String idMensajeria = mensajeriaService.enviarMailAvisoFaltasPagoProductor(productor, aviso);
                        aviso.setIdMensajeria(idMensajeria);
                        aviso.setEstado(Estado.A_ENVIAR);
                    } catch (EnvioEmailException e) {
                        logger.error("No se pudo enviar el mail para el productor " + productor.getNombre());
                        aviso.setEstado(Estado.FALLIDO);
                        aviso.setError(e.getMessage());
                    } catch (URLSharingBadGatewayException e) {
                        logger.error("No se pudo crear el mail de avisos de falta de pago para el productor " + productor.getNombre());
                        aviso.setEstado(Estado.FALLIDO);
                        aviso.setError(e.getMessage());
                    }
                    this.avisosDAO.guardarAviso(aviso);

                }
            }
//        }
        actualizarEstadosAEnviar();
    }

    public List<Aviso> getAvisos(){
        return avisosDAO.findAvisos();
    }

    public List<PolizaConDeuda> getPolizasConDeuda(String idProductor) {
        return glmDAO.obtenerPolizasConDeuda(idProductor);
    }





}
