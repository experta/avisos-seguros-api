package ar.com.experta.avisossegurosapi.core.model;

import ar.com.experta.avisossegurosapi.commons.exceptions.NotImplementedException;

import java.time.LocalDateTime;

public class AvisoFaltaPagoCliente extends Aviso{

    private PolizaConDeuda polizaConDeuda;

    public AvisoFaltaPagoCliente(TipoAviso tipoAviso) {
        super(tipoAviso);
    }

    public AvisoFaltaPagoCliente(String id, Estado estado, String idMensajeria, LocalDateTime fecha, String error, Integer reintento, PolizaConDeuda polizaConDeuda) {
        super(id,
                null,
                estado,
                idMensajeria,
                fecha,
                error,
                reintento);

        switch (polizaConDeuda.getFormaPago()){
            case CUPONERA:
                this.setTipo(TipoAviso.DEUDA_POLIZAS_CUPONERA_CLIENTES);
                break;
            case OPERATORIA_BANCARIA:
                this.setTipo(TipoAviso.DEUDA_POLIZAS_OPBANCARIA_CLIENTES);
                break;
            default:
                throw new NotImplementedException();
        }

        this.polizaConDeuda = polizaConDeuda;
    }

    public PolizaConDeuda getPolizaConDeuda() {
        return polizaConDeuda;
    }

    public void setPolizaConDeuda(PolizaConDeuda polizaConDeuda) {
        this.polizaConDeuda = polizaConDeuda;
    }
}
