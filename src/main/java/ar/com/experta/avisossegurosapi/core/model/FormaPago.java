package ar.com.experta.avisossegurosapi.core.model;

import java.util.HashMap;
import java.util.Map;

public enum FormaPago {
    CUPONERA (0, "Cuponera"),
    OPERATORIA_BANCARIA (2, "Operatoria Bancaria");

    private Integer codigo;
    private String label;

    private static Map<Integer, FormaPago> map = new HashMap<>();

    static {
        for (FormaPago ramo : FormaPago.values()) {
            map.put(ramo.codigo, ramo);
        }
    }

    FormaPago(Integer codigo, String label) {
        this.codigo = codigo;
        this.label = label;
    }

    public Integer codigo() {
        return codigo;
    }

    public String label() {
        return label;
    }

    public static FormaPago fromCodigo(final Integer codigo) {
        return map.get(codigo);
    }
}
