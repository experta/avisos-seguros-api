package ar.com.experta.avisossegurosapi.core.model;

import java.math.BigDecimal;
import java.time.LocalDate;

public class PolizaConDeuda {

    private Ramo ramo;
    private String numero;
    private Integer cuota;
    private LocalDate fechaVencimiento;
    private BigDecimal saldo;
    private Cliente cliente;
    private Productor productor;
    private FormaPago formaPago;

    public PolizaConDeuda() {
    }

    public PolizaConDeuda(Ramo ramo, String numero, Integer cuota, LocalDate fechaVencimiento, BigDecimal saldo, Cliente cliente, Productor productor, FormaPago formaPago) {
        this.ramo = ramo;
        this.numero = numero;
        this.cuota = cuota;
        this.fechaVencimiento = fechaVencimiento;
        this.saldo = saldo;
        this.cliente = cliente;
        this.productor = productor;
        this.formaPago = formaPago;
    }

    public Ramo getRamo() {
        return ramo;
    }

    public void setRamo(Ramo ramo) {
        this.ramo = ramo;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getCuota() {
        return cuota;
    }

    public void setCuota(Integer cuota) {
        this.cuota = cuota;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Productor getProductor() {
        return productor;
    }

    public void setProductor(Productor productor) {
        this.productor = productor;
    }

    public FormaPago getFormaPago() {
        return formaPago;
    }

    public void setFormaPago(FormaPago formaPago) {
        this.formaPago = formaPago;
    }

    public String getNumeroCompleto() {
        return ramo + "-" + numero;
    }

}
