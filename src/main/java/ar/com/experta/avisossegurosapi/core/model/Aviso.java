package ar.com.experta.avisossegurosapi.core.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public abstract class Aviso {

    private String id;
    private TipoAviso tipo;
    private Estado estado;
    private String idMensajeria;
    private LocalDateTime fecha;
    private String error;
    private Integer reintento;

    public Aviso() {
    }

    public Aviso(TipoAviso tipo) {
        this.tipo = tipo;
    }

    public Aviso(String id, TipoAviso tipo, Estado estado, String idMensajeria, LocalDateTime fecha, String error, Integer reintento) {
        this.id = id;
        this.tipo = tipo;
        this.estado = estado;
        this.idMensajeria = idMensajeria;
        this.fecha = fecha;
        this.error = error;
        this.reintento = reintento;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TipoAviso getTipo() {
        return tipo;
    }

    public void setTipo(TipoAviso tipo) {
        this.tipo = tipo;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getIdMensajeria() {
        return idMensajeria;
    }

    public void setIdMensajeria(String idMensajeria) {
        this.idMensajeria = idMensajeria;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Integer getReintento() {
        return reintento;
    }

    public void setReintento(Integer reintento) {
        this.reintento = reintento;
    }

    public void incrementarReintento(){
        reintento = reintento != null ? reintento.valueOf(reintento.intValue() + 1) : Integer.valueOf(1);
    }
}
