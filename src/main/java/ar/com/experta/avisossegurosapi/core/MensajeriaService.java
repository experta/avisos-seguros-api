package ar.com.experta.avisossegurosapi.core;

import ar.com.experta.avisossegurosapi.commons.exceptions.ConsultaEmailException;
import ar.com.experta.avisossegurosapi.commons.exceptions.EnvioEmailException;
import ar.com.experta.avisossegurosapi.commons.exceptions.NotImplementedException;
import ar.com.experta.avisossegurosapi.commons.exceptions.URLSharingBadGatewayException;
import ar.com.experta.avisossegurosapi.core.model.Aviso;
import ar.com.experta.avisossegurosapi.core.model.Estado;
import ar.com.experta.avisossegurosapi.core.model.PolizaConDeuda;
import ar.com.experta.avisossegurosapi.core.model.Productor;
import ar.com.experta.avisossegurosapi.infraestructure.mensajeriaclient.MensajeriaClient;
import ar.com.experta.avisossegurosapi.infraestructure.polizasclient.PolizasClient;
import ar.com.experta.avisossegurosapi.infraestructure.urlsharing.URLSharingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MensajeriaService {

    private static final Logger logger = LoggerFactory.getLogger(MensajeriaService.class);

    @Autowired
    private PolizasClient polizasClient;
    @Autowired
    private MensajeriaClient mensajeriaClient;
    @Autowired
    private URLSharingService urlSharingService;
    @Value("${url_local}")
    private String HOST_LOCAL;
    private String GET_REPORTE_PRODUCTOR = "/avisos/reportefaltapagocuponera/{productor}.xlsx";


    public Estado getEstado(String idMensajeria) throws ConsultaEmailException {
        return mensajeriaClient.getEstado(idMensajeria);
    }

    public String enviarMailAvisoFaltaPagoCliente(PolizaConDeuda poliza, Aviso aviso) throws EnvioEmailException {
        String plan = polizasClient.getPlan(poliza.getRamo(), poliza.getNumero());
        String campania;
        switch (aviso.getTipo()){
            case DEUDA_POLIZAS_CUPONERA_CLIENTES:
                campania = MensajeriaClient.TIPO_MAIL_FALTA_PAGO_CUPONERA_CLIENTE;
                break;
            case DEUDA_POLIZAS_OPBANCARIA_CLIENTES:
                campania = MensajeriaClient.TIPO_MAIL_FALTA_PAGO_OPBANCARIA_CLIENTE;
                break;
            default: throw new NotImplementedException();
        }

        return mensajeriaClient.enviarMailFaltaPagoClientes(
                campania,
                poliza.getCliente().getEmail(),
                poliza.getNumero(),
                plan,
                poliza.getCliente().getNombre(),
                poliza.getFechaVencimiento(),
                poliza.getSaldo());
    }

    public String enviarMailAvisoFaltasPagoProductor(Productor productor, Aviso aviso) throws EnvioEmailException, URLSharingBadGatewayException {
        String urlLocal = HOST_LOCAL + GET_REPORTE_PRODUCTOR;
        urlLocal = urlLocal.replace("{productor}", productor.getId());
        String link = urlSharingService.crearURLSharing(urlLocal).toString();
        return mensajeriaClient.enviarMailFaltaPagoCuponeraProductores(
                productor.getEmail(),
                productor.getNombre(),
                link);
    }

}
