package ar.com.experta.avisossegurosapi.core.model;

import java.util.HashMap;
import java.util.Map;

public enum TipoAviso {

    DEUDA_POLIZAS_CUPONERA_CLIENTES("1", "Aviso de Deuda de Polizas con Cuponera para Clientes", Boolean.TRUE, Boolean.FALSE, null),
    DEUDA_POLIZAS_PRODUCTOR("2", "Aviso de Reporte de Deudas de Polizas para Productores", Boolean.FALSE, Boolean.TRUE, 7L),
    DEUDA_POLIZAS_OPBANCARIA_CLIENTES("3", "Aviso de Deuda de Polizas con Op. Bancaria para Clientes", Boolean.TRUE, Boolean.FALSE, null);

    private String codigo;
    private String descripcion;
    private Boolean avisoCliente;
    private Boolean avisoProductor;
    private Long diasRepeticion;
    private static Map<String, TipoAviso> map = new HashMap<>();

    static {
        for (TipoAviso tipoAviso : TipoAviso.values()) {
            map.put(tipoAviso.codigo, tipoAviso);
        }
    }

    TipoAviso(String codigo, String descripcion, Boolean avisoCliente, Boolean avisoProductor, Long diasRepeticion) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.avisoCliente = avisoCliente;
        this.avisoProductor = avisoProductor;
        this.diasRepeticion = diasRepeticion;
    }

    public static TipoAviso fromValue(final String value) {
        return map.get(value);
    }

    public String codigo() {
        return codigo;
    }

    public String descripcion() {
        return descripcion;
    }

    public Boolean esAvisoCliente() {
        return avisoCliente;
    }

    public Boolean esAvisoProductor() {
        return avisoProductor;
    }

    public Long diasRepeticion() {
        return diasRepeticion;
    }
}
